Feature: FizzBuzz
  Scenario: I can process a single number
    Given I have the following matching conditions
      |0  |Fizz |3  |
      |1  |Buzz |5  |
    When I provide the number 5
    Then These conditions are met
      |0  |Fizz |3  |False |
      |1  |Buzz |5  |True  |

  Scenario: I can process a range of numbers
    Given I have the following matching conditions
      |0  |Fizz |3  |
      |1  |Buzz |5  |
    When I provide the numbers 0 to 50
    Then The following matches are made
      |3: Fizz        |
      |5: Buzz        |
      |6: Fizz        |
      |9: Fizz        |
      |10: Buzz       |
      |12: Fizz       |
      |15: Fizz Buzz  |
      |18: Fizz       |
      |20: Buzz       |
      |21: Fizz       |
      |24: Fizz       |
      |25: Buzz       |
      |27: Fizz       |
      |30: Fizz Buzz  |
      |33: Fizz       |
      |35: Buzz       |
      |36: Fizz       |
      |39: Fizz       |
      |40: Buzz       |
      |42: Fizz       |
      |45: Fizz Buzz  |
      |48: Fizz       |
      |50: Buzz       |