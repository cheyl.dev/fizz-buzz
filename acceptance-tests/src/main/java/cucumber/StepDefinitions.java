package cucumber;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import matcher.MatchCondition;
import matcher.Matcher;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

public class StepDefinitions {

    private Matcher matcher;
    private int numberToCheck;
    private int testRange;

    private static final int CONDITION_INDEX = 0;
    private static final int CONDITION_TEXT = 1;
    private static final int CONDITION_VALUE = 2;
    private static final int EXPECTED_MATCH = 3;

    @Given("I have the following matching conditions")
    public void setUpMatchConditionsTable(DataTable table) {
        matcher = new Matcher();
        for (List<String> row : table.asLists()) {
            matcher.addMatchCondition(new MatchCondition(Integer.parseInt(row.get(CONDITION_VALUE)), row.get(CONDITION_TEXT)));
        }

        assertThat(table.asLists().size(), is(matcher.getMatchConditions().size()));
    }

    @When("I provide the number {int}")
    public void getInputNumber(int input) {
        numberToCheck = input;
    }

    @Then("These conditions are met")
    public void checkAgainstConditions(DataTable table) {
        for (List<String> row : table.asLists()) {
            boolean shouldMatch;
            shouldMatch = row.get(EXPECTED_MATCH).equals("True");

            boolean doesMatch = matcher.conditionIsMet(Integer.parseInt(row.get(CONDITION_INDEX)), numberToCheck);
            assertThat(shouldMatch, is(doesMatch));
        }
    }

    @When("I provide the numbers 0 to {int}")
    public void getNumberRange(int input) {
        testRange = input;
    }

    @Then("The following matches are made")
    public void checkNumberRangeAgainstConditions(DataTable table) {
        List<String> matchesMade = processNumber(testRange);

        for (List<String> row : table.asLists()) {
            String expectedValue = row.get(CONDITION_INDEX);
            assertThat((matchesMade), hasItem(expectedValue));
        }

        assertThat(matchesMade.size(),is(table.asLists().size()));
    }

    public List<String> processNumber(int range) {

        List<String> processedBlock = new ArrayList<>();

        for (int i = 1; i <= range; i++) {
            StringBuilder output = new StringBuilder();
            boolean firstMatch = true;

            for (matcher.MatchCondition condition : matcher.getMatchConditions()) {
                if (condition.matchesCondition(i)) {
                    if (!firstMatch) {
                        output.append(" ");
                    }
                    output.append(condition.getName());

                    firstMatch = false;
                }
            }

            if (!output.toString().isEmpty()) {
                output.insert(0, i + ": ");
                processedBlock.add(output.toString());
            }
        }

        return processedBlock;
    }
}