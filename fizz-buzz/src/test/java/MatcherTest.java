import matcher.MatchCondition;
import matcher.Matcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MatcherTest {

    public static final int MATCHING_INPUT_VALUE = 18;
    public static final int NON_MATCHING_INPUT_VALUE = 17;
    public static final int VALID_CONDITION_INDEX = 0;
    public static final int INVALID_CONDITION_INDEX = 99;
    public static final MatchCondition TEST_CONDITION_1 = new MatchCondition(6, "test1");
    public static final MatchCondition TEST_CONDITION_2 = new MatchCondition(4, "test2");

    Matcher matcher;

    @BeforeEach
    void setup() {
        this.matcher = new Matcher();
    }

    @Test
    void conditionIsMet() {
        matcher.addMatchCondition(TEST_CONDITION_1);
        assertThat(matcher.conditionIsMet(VALID_CONDITION_INDEX, MATCHING_INPUT_VALUE), is(true));
    }

    @Test
    void conditionIsNotMet() {
        matcher.addMatchCondition(TEST_CONDITION_1);
        assertThat(matcher.conditionIsMet(VALID_CONDITION_INDEX, NON_MATCHING_INPUT_VALUE), is(false));
    }

    @Test
    void HandlesEmptyConditionList() {
        assertThat(matcher.conditionIsMet(INVALID_CONDITION_INDEX, MATCHING_INPUT_VALUE), is(false));
    }

    @Test
    void allConditionsAreMet() {
        matcher.addMatchCondition(TEST_CONDITION_1);
        matcher.addMatchCondition(TEST_CONDITION_2);
        assertThat(matcher.allConditionsAreMet(12), is(true));
    }

    @Test
    void allConditionsAreNotMet() {
        matcher.addMatchCondition(TEST_CONDITION_1);
        matcher.addMatchCondition(TEST_CONDITION_2);
        assertThat(matcher.allConditionsAreMet(11), is(false));
    }
}