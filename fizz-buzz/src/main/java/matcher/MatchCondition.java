package matcher;

public class MatchCondition {

    private String name;
    private int number;

    public MatchCondition() {
    }

    public MatchCondition(int number, String name) {
        this.number = number;
        this.name = name;
    }

    public boolean matchesCondition(final int input) {
        return input % number == 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "MatchCondition{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }
}