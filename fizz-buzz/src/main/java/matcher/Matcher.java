package matcher;

import java.util.ArrayList;
import java.util.List;

public class Matcher {

    private List<MatchCondition> matchConditions;

    public Matcher() {
        this.matchConditions = new ArrayList<>();
    }

    public boolean conditionIsMet(int conditionIndex, int input) {
        if (matchConditions.size() > 0) {
            return matchConditions.get(conditionIndex).matchesCondition(input);
        } else
            return false;
    }

    public boolean allConditionsAreMet(int input) {
        for (MatchCondition condition : matchConditions) {
            if (!condition.matchesCondition(input)) {
                return false;
            }
        }
        return true;
    }

    public List<MatchCondition> getMatchConditions() {
        return matchConditions;
    }

    public void setMatchConditions(List<MatchCondition> matchConditions) {
        this.matchConditions = matchConditions;
    }

    public void addMatchCondition(MatchCondition matchCondition) {
        matchConditions.add(matchCondition);
    }

    @Override
    public String toString() {
        return "Matcher{" +
                "matchConditions=" + matchConditions +
                '}';
    }
}