# Ubuntu Provisioning Script
This repository is my solution to the classic "Fizz Buzz" problem.
The solution is over-engineered because it acts as a demonstration of my ability to produce hexagonally architected Java code, built with Gradle and tested used Cucumber.

## The Task
If you don't know already, the "Fizz Buzz" problem is a simple task that many programmers solve for technical tests or early code practice.
When you provide an integer,
- If it is **divisible by 3** then the output should be "**Fizz**"
- If it is **divisible by 5** then the output should be "**Buzz**"
- If it is **divisible by 3 and 5** then the output should be "**Fizz Buzz**"

## Prerequisites
- Java 11

## How to Build
###Terminal
```
./gradlew clean build
```
###IntelliJ Idea
- Open the **gradle tab**
- Double-click the "**clean**" task under **fizz-buzz > build**
- Double-click the "**build**" task under **fizz-buzz > build**

## How To Run Tests
###IntelliJ Idea
- Navigate to **/home/colin/IdeaProjects/fizz-buzz/acceptance-tests/src/main/java/cucumber/CucumberRunner.java**
- Right click in the project tab
  - Select "**Run CucumberRunner**"

###Troubleshooting
#### No tasks available
If you run into "no tasks available" whilst trying to run Cucumber Runner:
- Go to **File > Settings**
- Go to **Build, Execution, Deployment > Build Tools > Gradle**
- Under both "**Build and Run Using**", and "**Run tests using**", select "**IntelliJ IDEA**"